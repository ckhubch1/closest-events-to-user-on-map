import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class Driver
{
	public static Hashtable<Integer, Event> eventDetails = new Hashtable<Integer, Event>();
	public static Map<Integer, Event> eventDistance = new TreeMap<Integer, Event>();
	public static int xInput;
	public static int yInput;
	public static int totalEvents;

	public static int generateRandomNumbers(long seed)
	{
		int random;
		Random generator = new Random(seed);
		random = generator.nextInt();
		return random;
	}

	public static boolean checkEventExists(int x, int y)
	{
		Set<Integer> keys = eventDetails.keySet();
		for (Integer key : keys)
		{
			if (eventDetails.get(key).xCoordinate == x && eventDetails.get(key).yCoordinate == y)
			{
				return true;
			}
		}
		return false;

	}

	public static void createEvents(int totalEvents)
	{
		Event event;

		// Change your seed value here
		long seed = -50;

		for (int eventNo = 1; eventNo <= totalEvents; eventNo++)
		{
			int xCoord = generateRandomNumbers(seed) % 10;
			int yCoord = generateRandomNumbers(seed * 2) % 10;
			int noOfTickets = Math.abs(generateRandomNumbers(seed * 3) % 100);
			ArrayList<Double> priceOfTickets = new ArrayList<Double>();

			for (int i = 0; i < noOfTickets; i++)
			{
				double price = (double) Math.round(Math.abs(generateRandomNumbers(seed * 4) % 100.15) * 1000d) / 1000d
						+ 1;
				priceOfTickets.add(price);
				seed = seed * 2;
			}

			/* Sort priceOfTickets to get lowest price on top */
			Collections.sort(priceOfTickets);

			/* Check if event already exists in a position */
			if (checkEventExists(xCoord, yCoord) == true)
			{
				/* If event exists generate new position for event */
				do
				{
					xCoord = generateRandomNumbers(seed * -1) % 10;
					yCoord = generateRandomNumbers(seed * 2) % 10;
					seed--;
				} while (checkEventExists(xCoord, yCoord) == true);
				/* Create Event and add it to a Hashtable */
				event = new Event(eventNo, xCoord, yCoord, noOfTickets, priceOfTickets);
				eventDetails.put(eventNo, event);
			} else /* Event does not exist in that position */
			{
				event = new Event(eventNo, xCoord, yCoord, noOfTickets, priceOfTickets);
				eventDetails.put(eventNo, event);
			}
			seed--; // Keep changing seed to get different combination of
					// numbers
		}
	}

	public static int manhattanFormula(int xCoordIn, int yCoordIn)
	{
		int value = Math.abs(xInput - xCoordIn) + Math.abs(yInput - yCoordIn);
		return value;
	}

	public static void calculateDistance()
	{
		Set<Integer> keys = eventDetails.keySet();
		for (Integer key : keys)
		{
			int val = manhattanFormula(eventDetails.get(key).xCoordinate, eventDetails.get(key).yCoordinate);
			eventDistance.put(val, eventDetails.get(key));
		}

	}

	public static void printClosestEvents(int noOfEvents)
	{
		System.out.println("\n" + noOfEvents + " closest Events to (" + xInput + "," + yInput + "):");
		Set<Integer> keys = eventDistance.keySet();
		for (Integer key : keys)
		{
			if (noOfEvents != 0)
			{
				System.out.println("Event: " + eventDistance.get(key).eventID + " - $"
						+ eventDistance.get(key).price.get(0) + " , Distance: " + key + " units.");
				noOfEvents--;
			} else
				break;
		}

	}

	public static void printAllEvents()
	{
		System.out.println("\nAll events and Details:\n");
		Set<Integer> keys = eventDistance.keySet();
		for (Integer key : keys)
		{
			System.out.println("EventID-> " + eventDistance.get(key).eventID + ", Location: ("
					+ eventDistance.get(key).xCoordinate + "," + eventDistance.get(key).yCoordinate
					+ "), Distance from user: " + key + ", No.Of Tickets: " + eventDistance.get(key).totalTickets
					+ ", All Ticket Prices(Sorted): $" + eventDistance.get(key).price);
		}
	}

	public static void main(String[] args)
	{
		/* Generating events with random seed */
		int totalEvents = Math.abs(generateRandomNumbers(System.currentTimeMillis()) % 20) + 6;
		System.out.println("Generating " + totalEvents + " Random Events using random seed...\n");
		createEvents(totalEvents);

		/* Taking input from user using Scanner */
		Scanner sc = new Scanner(System.in);
		System.out.print("Please input your Coordinates:\n>");
		String input = sc.next();
		while(!(input.matches(".*,.*")))
		{
			System.out.print("Please input your Coordinates in the correct format:\n>");
			input = sc.next();
		}
		String[] parts = input.split(",");
		xInput = Integer.parseInt(parts[0]);
		yInput = Integer.parseInt(parts[1]);
		/* Checking if user is within the given world range */
		while (!(xInput >= -10 && xInput <= 10 && yInput >= -10 && yInput <= 10))
		{
			System.out.println("Please re-enter your coordinates within the world range");
			System.out.print("Please input your Coordinates:\n>");
			input = sc.next();
			parts = input.split(",");
			xInput = Integer.parseInt(parts[0]);
			yInput = Integer.parseInt(parts[1]);
		}
		sc.close();

		calculateDistance(); /* Calculate distances from user to all events */

		/* Uncomment to view all events */
		// printAllEvents();

		/* Find 5 closest events to user coordinates */
		printClosestEvents(5);

	}

}
