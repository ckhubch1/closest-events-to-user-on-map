import java.util.ArrayList;

public class Event
{
	int eventID;
	int xCoordinate;
	int yCoordinate;
	int totalTickets;
	ArrayList<Double> price = new ArrayList<Double>();

	public Event(int eventID, int xCoordinate, int yCoordinate, int totalTickets, ArrayList<Double> price)
	{
		this.eventID = eventID;
		this.xCoordinate = xCoordinate;
		this.yCoordinate = yCoordinate;
		this.totalTickets = totalTickets;
		this.price = price;
	}

}
